resource "aws_launch_configuration" "testsite" {
  lifecycle { create_before_destroy = true }
  image_id       = "${var.ami-id}"
  instance_type  = "${var.ec2-type}"
  key_name       = "${var.ssh_key}"
  security_group = ["${aws_security_group.test_site_acl.arn}"]
}

resource "aws_autoscaling_group" "testsiteone" {
  lifecycle { create_before_destroy = true }
  name                 = "testsite - ${aws_launch_configuration.testsite.name}"
  launch_configuration = "${aws_launch_configuration.testsite.name}"
  desired_capacity     = "${var.nodes}"
  min_size             = "${var.nodes}"
  max_size             = "${var.nodes}"
  min_elb_capacity     = "${var.nodes}"
  availability_zones   = ["${data.aws_availability_zones.available.names}"]
  vpc_zone_identifier  = ["${var.vpc_id}"]
  load_balancers       = ["${aws_lb.test-site.id}"]
  tag {
    key                 = "sitegroup"
    value               = "one"
    propagate_at_launch = true
  }
}


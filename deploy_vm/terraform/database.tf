resource "aws_rds_cluster_instance" "cluster_instances" {
  count              = "${var.db_count}"
  identifier         = "aurora-cluster-${count.index}"
  cluster_identifier = "${aws_rds_cluster.site.id}"
  instance_class     = "db.r4.large"
}

resource "aws_rds_cluster" "site" {
  cluster_identifier = "aurora-cluster"
  availability_zones = ["${data.aws_availability_zones.available.names}"]
  database_name      = "${var.db_name}"
  master_username    = "${var.db_user}"
  master_password    = "${var.db_passwd}"
}

variable "region" {
  default = "eu-central-1"
}

variable "ami-id" {
  default = "ami-0dd0be70cc0d493b7"
}

variable "ec2-type" {
  default = "t2.small"
}
variable "vpc_id" {
  default = "vpc-c01fe6ab"
}

variable "ssh_key" {
  default = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQC3rqcrr02JSd3WIKuBnbHHbZVUbrajNaelEulSq9Hp3gkWg7YAI36Ne/lzoMDL3+OZQtpr4Parjq4QuXLVGc/Qwba0BD1E1mj5zhPLPXLf19Ik+ucUUq1CempEDjpQHsUZlP/q1WmGPxyxFUS2b0HM1Q81GDcfeE2wnurh7i0sis0kSh+ArFVeEGMK8ngJoAu/RE5MXLIRR0FXWt+sMrYcWsCPoD26NxE4qaFVe230VcabDnQy8zONi4M3Wb0WznOWnyW3xFIA1l1atQrGbIv2Yfxv4ZibWm9qyjPitoh1GKnErBD9dgSNzMaVB9xHdYz6v8OzBK3N9T34sces97W3 root@ubuntu"
}
variable "bastion_host" {
  default = "0.0.0.0/0"
}

variable "backend_num" {
  default = "2"
}

variable "node_prefix" {
  default = "test00"
}

variable "nodes" {
  default = "1"
}

variable "github_token" {
  default = "blablabla"
}

variable "db_count" {
  default = "2"
}
variable "db_name" {
  default = "site"
}
variable "db_user" {
  default = "user"
}
variable "db_passwd" {
  default = "password"
}

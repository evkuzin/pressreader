provider "aws" {
  region = "${var.region}"
}
data "aws_vpc" "main" {
  id = "${var.vpc_id}"
}

resource "aws_key_pair" "test_key" {
  key_name = "test_key"
  public_key = "${var.ssh_key}"
}
resource "aws_security_group" "test_site_acl" {
  name = "test site acl"
  description = "test site acl"
  ingress {
    from_port = 22
    to_port = 22
    protocol = "tcp"
    cidr_blocks = ["${var.bastion_host}"]
  }
  ingress {
    from_port = 8000
    to_port = 8000
    protocol = "tcp"
    security_groups = ["${aws_security_group.test_site_lb.id}"]
  }
  ingress {
    from_port = -1
    to_port = -1
    protocol = "icmp"
    self = true
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

resource "aws_security_group" "test_site_lb" {
  name = "test site lb acl"
  description = "test site acl"
  ingress {
    from_port = 80
    to_port = 80
    protocol = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
}

data "aws_availability_zones" "available" {}
 


resource "aws_lb" "test-site" {
  name               = "test-site"
  internal           = false
  load_balancer_type = "application"
  security_groups    = ["${aws_security_group.test_site_lb.id}"]
  subnets            = ["${data.aws_subnet_ids.subnets.ids}"]
}
data "aws_subnet_ids" "subnets" {
  vpc_id = "${var.vpc_id}"
}
resource "aws_lb_target_group" "test-site" {
  name     = "test-site-lb-tg"
  port     = 8000
  protocol = "HTTP"
  vpc_id   = "${data.aws_vpc.main.id}"
  health_check {
    port = 8000
    path = "/"
  }
}

resource "aws_lb_listener" "test_site_http" {
  load_balancer_arn = "${aws_lb.test-site.arn}"
  port              = "80"
  protocol          = "HTTP"

  default_action {
    type = "forward"
    target_group_arn = "${aws_lb_target_group.test-site.arn}"
  }
}

resource "aws_lb_listener_rule" "site" {
  listener_arn = "${aws_lb_listener.test_site_http.arn}"
  priority     = 100

  action {
    type             = "forward"
    target_group_arn = "${aws_lb_target_group.test-site.arn}"
  }

  condition {
    field  = "path-pattern"
    values = ["/hello/*"]
  }
}



#!/usr/bin/python3
# -*- coding: utf-8 -*-

import logging
from flask import Flask, flash, render_template, request, jsonify
from werkzeug.exceptions import default_exceptions
from werkzeug.contrib.fixers import ProxyFix
import os, sys
from http import HTTPStatus
from datetime import datetime
import sentry_sdk
from sentry_sdk.integrations.flask import FlaskIntegration
from sqlalchemy import create_engine, Table, Column, String, DateTime
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base

#TODO after DB creation change DB url to external mysql/postgres
engine = create_engine('sqlite:////opt/site/site.db', convert_unicode=True)
db_session = scoped_session(sessionmaker(autocommit=False,
                                         autoflush=False,
                                         bind=engine))

Base = declarative_base()
Base.query = db_session.query_property()

class site(Base):

    __tablename__ = 'site_data'
    username = Column(String, primary_key=True)
    date = Column(DateTime)

    def __repr__(self):
        return '<{0}>'.format(self.username)


def days_to_x(b_date, now):
    delta1 = datetime(now.year, b_date.month, b_date.day)
    delta2 = datetime(now.year+1, b_date.month, b_date.day)
    days = (max(delta1, delta2) - now).days

    return days


def eprint(data):
    print("==========================\n" + data + "\n==========================", file=sys.stderr)

def insert_or_update(username, date):
    user = db_session.query(site).filter(site.username == username).first()
    if not user:
       user = site(username=username, date=datetime.strptime(date, "%Y-%d-%m"))
    else:
       user.date = datetime.strptime(date, "%Y-%d-%m")
    return user




sentry_sdk.init(
    dsn="https://78826126b3f24f9d8e6142bdf73340ce@sentry.io/1334407",
    integrations=[FlaskIntegration()]
)

app = Flask(__name__)
app.wsgi_app = ProxyFix(app.wsgi_app)
if not os.path.isfile('/opt/site/site.db'):
    Base.metadata.create_all(bind=engine)


#TODO send it to ELK
logging.basicConfig(filename='/dev/stdout', level=logging.DEBUG, format='%(asctime)s %(message)s')


@app.route("/hello/<username>", methods=["GET", "POST"])
def hello(username):
    try:
        if request.method == "POST":
            #TODO try async solution
            date = request.json["dateOfBirth"]
            user = insert_or_update(username, date)
            db_session.add(user)
            db_session.commit()
            return ('', 204)
        else:	
            now = datetime.now()
            q = db_session.query(site).filter(site.username == username).first()
            if q is not None:
                if q.date != now:
                    resp = {"message": "Hello, {}! Your birthday is in {} days!".format(username, days_to_x(q.date, datetime.now()))}
                    return (jsonify(resp), 200)
                else: 
                    resp = {"message": "Hello, {}! Happy birthday!".format(username)}
                    return (jsonify(resp), 200)
            else:
                return ('', 404)
    except Exception as E:
        return (str(E), 500)

#!/usr/bin/python3

from sqlalchemy import create_engine, Table, Column, String, DateTime
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base



engine = create_engine('sqlite:////opt/site/site.db', convert_unicode=True)
db_session = scoped_session(sessionmaker(autocommit=False,
                                         autoflush=False,
                                         bind=engine))
Base = declarative_base()
Base.query = db_session.query_property()



class site(Base):

    __tablename__ = 'site_data'
    username = Column(String, primary_key=True)
    date = Column(DateTime)

    def __repr__(self):
        return '<{0}>'.format(self.username)


def init_db():
    # import all modules here that might define models so that
    # they will be registered properly on the metadata.  Otherwise
    # you will have to import them first before calling init_db()
#    import models
    Base.metadata.create_all(bind=engine)

if __name__ == "__main__":
    init_db()

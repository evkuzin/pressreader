output "aws_lb_url" {
  value = ["${aws_lb.test-site.dns_name}"]
}

output "cluster_endpoint" {
  value = "${aws_rds_cluster.site.endpoint}"
}